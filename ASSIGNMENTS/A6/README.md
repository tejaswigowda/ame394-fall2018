# Assignment 6
## Due Oct 29<sup>th</sup> in class 

Building on the solution to A5, make sure an alert email is sent whenever the temperature is reported to be 100&deg; F. Make sure that the emails are sent atleast 5 mins apart.


### References

- [NodeMailer](https://github.com/nodemailer/nodemailer).
- [NodeMailer + Gmail](https://community.nodemailer.com/using-gmail/).
