# ame394-fall2018

## Required Hardware

[IoT Kit](https://www.amazon.com/OSOYOO-Programming-Learning-Developmen-Tutorial/dp/B073Z84243/ref=sr_1_1)
## Required Software

[esp8266 Arduino](https://github.com/esp8266/Arduino)

[ESP8266 board definition](http://arduino.esp8266.com/versions/2.4.2/package_esp8266com_index.json)

Driver for Mac
[SI Labs](https://www.silabs.com/Support%20Documents/Software/Mac_OSX_VCP_Driver.zip)
[CH340](https://sparks.gogo.co.nz/ch340.html)

```
sudo iptables -A PREROUTING -t nat -p tcp --dport 80 -j REDIRECT --to-ports 1234

```
