
int led1 = D2;           
int led2 = D5;           
int led3 = D6;           
int led4 = D8;           

int brightness = 0;    // how bright the LED is
int fadeAmount = 5;    // how many points to fade the LED by

// the setup routine runs once when you press reset:
void setup() {
  pinMode(led1, OUTPUT);
  pinMode(led2, OUTPUT);
  pinMode(led3, OUTPUT);
  pinMode(led4, OUTPUT);
  Serial.begin(115200);
}

// the loop routine runs over and over again forever:
void loop() {
  Serial.println(millis());
  int ledVal1 = map(brightness, 0, 1024, 0, 1024);
  analogWrite(led1, ledVal1);
  
  int ledVal2 = map(brightness, 1024, 2048, 0, 1024);
  analogWrite(led2, ledVal2);
  
  int ledVal3 = map(brightness, 2048, 3072, 0, 1024);
  analogWrite(led3, ledVal3);

  int ledVal4 = map(brightness, 3072, 4096, 0, 1024);
  analogWrite(led4, ledVal4);
  
  // change the brightness for next time through the loop:
  brightness = brightness + fadeAmount;

  // reverse the direction of the fading at the ends of the fade:
  if (brightness <= 0 || brightness >= 4096) {
    fadeAmount = -fadeAmount;
  }
  // wait for 30 milliseconds to see the dimming effect
  delay(30);
}
