#include <Wire.h> 
#include <LiquidCrystal_I2C.h>

// Set the LCD address to 0x27 for a 16 chars and 2 line display
LiquidCrystal_I2C lcd(0x27, 16, 2);
//Connect to D1 and D2 on nodemcu


void setup()
{
	// initialize the LCD
	lcd.begin();
  print2Screen("Test line 1", "Test line 2");
}

void print2Screen(String s1, String s2){
  lcd.backlight();
  lcd.print(s1);
  lcd.setCursor(0,1);
  lcd.print(s2);
}
void loop()
{
	// Do nothing here...
}
